// Reading commands directory and identify files
const fs = require('node:fs');
// Helps the construction of paths
const path = require('node:path');
// Necessary discord.js classes
const { Client, Collection, Events, GatewayIntentBits } = require('discord.js');

// Environement
const { token } = require('./config.json');

// Client
const client = new Client({ intents: [GatewayIntentBits.Guilds] });
client.commands = new Collection();

// Retrieve commands
const foldersPath = path.join(__dirname, 'commands');
const commandsFolder = fs.readdirSync(foldersPath);

// Check all commands subfolders
for (const folder of commandsFolder) {
    const commandsPath = path.join(foldersPath, folder);
    const commandsFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

    // Check all files of all subfolders
    for (const file of commandsFiles) {
        const filePath = path.join(commandsPath, file);
        const command = require(filePath);

        // Add to commands list if valid
        if ('data' in command && 'execute' in command) {
            client.commands.set(command.data.name, command);
        }
        else {
            console.log(`ERROR: ${filePath}, check data or execute.`);
        }
    }
}

// Reading all events files
const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter(file => file.endsWith('.js'));

for (const file of eventFiles) {
    const filePath = path.join(eventsPath, file);
    const event = require(filePath);
    if (event.once) {
        client.once(event.name, (...args) => event.execute(...args));
    } else {
        client.on(event.name, (...args) => event.execute(...args));
    }
}

// Log in to Discord with your client's token
client.login(token);